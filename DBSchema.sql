CREATE DATABASE ResourceMonitoring;

CREATE LOGIN MonitoringUser WITH PASSWORD='Performance123', DEFAULT_DATABASE=ResourceMonitoring, DEFAULT_LANGUAGE=Russian
GO

ALTER SERVER ROLE [sysadmin] ADD MEMBER MonitoringUser
GO

USE ResourceMonitoring
GO

CREATE TABLE Computers(
Id int NOT NULL IDENTITY,
Name nvarchar(100),
PRIMARY KEY (Id)
)
GO

CREATE TABLE PerformanceData(
MeasureTimestamp datetime,
CPU int,
UsedMemory int,
DiskUsage int,
ComputerId int,
PRIMARY KEY (MeasureTimestamp, ComputerId),
FOREIGN KEY (ComputerId) REFERENCES Computers(Id)
)
GO

CREATE PROCEDURE AddComputer
@name char(100)
AS
INSERT INTO Computers (Name)
VALUES(@name)
SELECT SCOPE_IDENTITY()
GO

CREATE PROCEDURE AddData
@MMeasTime datetime,
@MCPU int,
@MMemory int,
@MDisk int,
@MComputerId int
AS
INSERT INTO PerformanceData
(MeasureTimestamp,CPU,UsedMemory,DiskUsage,ComputerId)
VALUES(@MMeasTime,@MCPU,@MMemory,@MDisk,@MComputerId)
GO