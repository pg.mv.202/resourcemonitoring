﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Configuration;
using System.Runtime.InteropServices;
using System.Globalization;
using System.IO;
using System.Threading;


namespace MonitoringService
{

    static class Program
    {
        [DllImport("kernel32.dll")]
        static extern IntPtr GetConsoleWindow();

        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        const int SW_HIDE = 0;
        const int SW_SHOW = 5;
        static void Main()
        {
            var handle = GetConsoleWindow();
            ShowWindow(handle, SW_SHOW);
            Service1 service = new Service1();
            if (Environment.UserInteractive)
            {
                ShowWindow(handle, SW_SHOW);
                try
                {
                    ShowRecentRows();
                }
                catch(Exception e)
                {
                    Console.WriteLine("Can't read old data");
                }
                Console.ReadLine();
            }
            else
            {
                
                ShowWindow(handle, SW_HIDE);
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[]
                {
                service
                };
                ServiceBase.Run(ServicesToRun);
            }
        }
        private static void ShowRecentRows()
        {
            string sqlExpression = "SELECT TOP (100) MeasureTimestamp, CPU, UsedMemory, DiskUsage FROM PerformanceData JOIN Computers ON Id = ComputerId WHERE Id = (SELECT Id FROM Computers WHERE Name='" + ConnectParameteres.PCName + "') ORDER BY MeasureTimestamp DESC;";
            using (SqlConnection connection = new SqlConnection(ConnectParameteres.connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    Console.WriteLine("{0}\t{1}\t{2}\t{3}", reader.GetName(0), reader.GetName(1), reader.GetName(2), reader.GetName(3));

                    while (reader.Read())
                    {
                        object timestamp = reader["MeasureTimestamp"];
                        object cpu = reader["CPU"];
                        object memory = reader["UsedMemory"];
                        object disk = reader["DiskUsage"];

                        Console.WriteLine("{0} \t{1} \t{2}\t{3}", timestamp, cpu, memory, disk);
                    }
                }

                reader.Close();
            }
        }
    }
}
