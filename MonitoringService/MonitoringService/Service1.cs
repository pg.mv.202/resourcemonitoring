﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Data.SqlClient;
using System.Globalization;
using System.Configuration;
using System.IO;
using System.Runtime.InteropServices;
using System.Management;

namespace MonitoringService
{
    public partial class Service1 : ServiceBase
    {
        MonitoringLogger monlogger;
        public Service1()
        {
            InitializeComponent();
            this.CanStop = true;
            this.CanPauseAndContinue = true;
            this.AutoLog = true;
            
        }

        protected override void OnStart(string[] args)
        {
            monlogger = new MonitoringLogger();
            Thread loggerThread = new Thread(new ThreadStart(monlogger.Start));
            loggerThread.Start();
            
        }

        protected override void OnStop()
        {
            monlogger.Stop();
        }
    }

    class MonitoringLogger
    {
        

        private readonly PerformanceCounter cpuCounter = new PerformanceCounter("Processor", "% Processor Time", "_Total");
        private readonly PerformanceCounter ramCounter = new PerformanceCounter("Memory", "Available MBytes");
        private readonly PerformanceCounter diskCounter = new PerformanceCounter("LogicalDisk", "% Free Space", "C:");
        private int id;
        private readonly int delay = int.Parse(ConfigurationManager.AppSettings["TransmitDelay"]);
        
        private bool enabled = false;
        public MonitoringLogger()
        {
            id = AddComputer(ConnectParameteres.PCName);
        }
        public void Start()
        {
            
            CultureInfo.CurrentCulture = CultureInfo.CreateSpecificCulture("ru-RU");
            getCurrentCpuUsage();
            
            Thread.Sleep(1000);
            
            enabled = true;
            Run();
        }
        public void Stop()
        {
            enabled = false;
        }
        public void Run()
        {
            while (enabled)
            {
                AddData(DateTime.UtcNow, getCurrentCpuUsage(), getMemoryUsage(), getDiskUsage(), id);
                Thread.Sleep(delay * 1000);
            }
        }
        private int getCurrentCpuUsage()
        {
            return Convert.ToInt32(cpuCounter.NextValue());
        }

        private int getAvailableRAM()
        {
            return Convert.ToInt32(ramCounter.NextValue());
        }
        private int getFreeDiskSpace()
        {
            return Convert.ToInt32(diskCounter.NextValue());
        }
        private int getDiskUsage()
        {
            return 100 - getFreeDiskSpace();
        }
        private int getMemoryUsage()
        {
            return GetPhysicalMemory() - getAvailableRAM();
        }
        public static int GetPhysicalMemory()
        {
            ManagementScope oMs = new ManagementScope();
            ObjectQuery oQuery = new ObjectQuery("SELECT Capacity FROM Win32_PhysicalMemory");
            ManagementObjectSearcher oSearcher = new ManagementObjectSearcher(oMs, oQuery);
            ManagementObjectCollection oCollection = oSearcher.Get();

            long MemSize = 0;
            long mCap = 0;

            foreach (ManagementObject obj in oCollection)
            {
                mCap = Convert.ToInt64(obj["Capacity"]);
                MemSize += mCap;
            }
            MemSize = (MemSize / 1024) / 1024;
            return Convert.ToInt32(MemSize);
        }
        private int AddComputer(string name)
        {
            string sqlExpression = "AddComputer";

            using (SqlConnection connection = new SqlConnection(ConnectParameteres.connectionString))
            {
                connection.Open();
                
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                command.CommandType = System.Data.CommandType.StoredProcedure;
                
                string sqlExpressionFind = "SELECT Id FROM Computers WHERE Name='" + name + "';";
                SqlCommand commandFind = new SqlCommand(sqlExpressionFind, connection);
                int number = Convert.ToInt32(commandFind.ExecuteScalar());
                if (number > 0)
                {
                    connection.Close();
                    return number;
                }
                else
                {
                    SqlParameter nameParam = new SqlParameter
                    {
                        ParameterName = "@name",
                        Value = name
                    };
                    command.Parameters.Add(nameParam);
                    int result = Convert.ToInt32(command.ExecuteScalar());
                    connection.Close();
                    return result;
                }
            }
        }
        private void AddData(DateTime timestamp, int cpu, int memory, int disk, int pcId)
        {
            string sqlExpression = "AddData";

            using (SqlConnection connection = new SqlConnection(ConnectParameteres.connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                command.CommandType = System.Data.CommandType.StoredProcedure;


                SqlParameter timeParam = new SqlParameter
                {
                    ParameterName = "@MMeasTime",
                    Value = timestamp.ToString()
                };
                command.Parameters.Add(timeParam);
                SqlParameter cpuParam = new SqlParameter
                {
                    ParameterName = "@MCPU",
                    Value = cpu
                };
                command.Parameters.Add(cpuParam);
                SqlParameter memoryParam = new SqlParameter
                {
                    ParameterName = "@MMemory",
                    Value = memory
                };
                command.Parameters.Add(memoryParam);
                SqlParameter diskParam = new SqlParameter
                {
                    ParameterName = "@MDisk",
                    Value = disk
                };
                command.Parameters.Add(diskParam);
                SqlParameter idParam = new SqlParameter
                {
                    ParameterName = "@MComputerId",
                    Value = pcId
                };
                command.Parameters.Add(idParam);

                command.ExecuteNonQuery();
            }
        }
        
    }

    static class ConnectParameteres
    {
        public static readonly string connectionString = ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString;
        public static readonly string PCName = Environment.MachineName;
    }
}
